package main

import (
	"io"
	"net/http"
	"path"
	"strconv"
	"time"

	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/rs/zerolog/log"
)

// NewSSLRedirectHandler will redirect url to https version if it is http
func NewSSLRedirectHandler(next http.Handler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Scheme != "https" {
			dest := "https://" + r.Host + r.URL.Path
			if r.URL.RawQuery != "" {
				dest += "?" + r.URL.RawQuery
			}

			http.Redirect(w, r, dest, http.StatusTemporaryRedirect)
			return
		}

		next.ServeHTTP(w, r)
	}
}

// HostDispatchingHandler struct
type HostDispatchingHandler interface {
	HandleHost(host string, handler http.Handler)
	ServeHTTP(w http.ResponseWriter, r *http.Request)
}

type realHostDispatchingHandler struct {
	hosts map[string]http.Handler
}

// NewHostDispatchingHandler creates a HostDispatchingHandler
func NewHostDispatchingHandler() HostDispatchingHandler {
	return &realHostDispatchingHandler{
		hosts: make(map[string]http.Handler),
	}
}

// HandleHost allow for handler on host
func (h *realHostDispatchingHandler) HandleHost(host string, handler http.Handler) {
	h.hosts[host] = handler
}

func (h *realHostDispatchingHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	handler, ok := h.hosts[getHost(r)]
	if !ok {
		http.Error(w, "Not Found", http.StatusNotFound)
		return
	}

	handler.ServeHTTP(w, r)
}

// NewBasicAuthHandler handler for basic auth protection
func NewBasicAuthHandler(users []User, next http.Handler) http.HandlerFunc {
	m := make(map[string]string)
	for _, u := range users {
		m[u.Name] = u.Password
	}

	return func(w http.ResponseWriter, r *http.Request) {
		username, password, ok := r.BasicAuth()
		if !ok {
			challenge(w, r)
			return
		}

		p, ok := m[username]
		if !ok {
			challenge(w, r)
			return
		}

		if password != p {
			challenge(w, r)
			return
		}

		next.ServeHTTP(w, r)
	}
}

// NewWebsiteHandler website handler
func NewWebsiteHandler(next http.Handler, cfg *s3.GetBucketWebsiteOutput) http.HandlerFunc {
	suffix := cfg.IndexDocument.Suffix

	return func(w http.ResponseWriter, r *http.Request) {
		path := r.URL.Path

		if path == "" || path[len(path)-1] == '/' {
			r.URL.Path += *suffix
		}

		next.ServeHTTP(w, r)
	}
}

// NewProxyHandler handle proxy requests
func NewProxyHandler(proxy S3Proxy, prefix string) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var basePath string
		if prefix != "" {
			basePath = path.Join(prefix, r.URL.Path)
		}

		for {
			obj, err := proxy.Get(basePath)

			if err != nil {
				if awsErr, ok := err.(awserr.Error); ok {
					switch awsErr.Code() {
					case s3.ErrCodeNoSuchBucket, s3.ErrCodeNoSuchKey:
						notfound := proxy.GetOptions().NotFoundURI

						if notfound != "" {
							p := path.Join(prefix, notfound)
							if p != basePath {
								log.Info().Msgf("'%s' is not found try '%s' instead", basePath, p)
								basePath = p
								continue
							}
							http.Error(w, "circular URI retry", http.StatusNotFound)
						}

						http.Error(w, err.Error(), http.StatusNotFound)

					default:
						http.Error(w, err.Error(), http.StatusUnauthorized)
					}
				} else {
					http.Error(w, err.Error(), http.StatusInternalServerError)
				}

				break
			}

			setHeader(w, "Cache-Control", s2s(obj.CacheControl))
			setHeader(w, "Content-Disposition", s2s(obj.ContentDisposition))
			setHeader(w, "Content-Encoding", s2s(obj.ContentEncoding))
			setHeader(w, "Content-Language", s2s(obj.ContentLanguage))
			setHeader(w, "Content-Length", i2s(obj.ContentLength))
			setHeader(w, "Content-Range", s2s(obj.ContentRange))
			setHeader(w, "Content-Type", s2s(obj.ContentType))
			setHeader(w, "ETag", s2s(obj.ETag))
			setHeader(w, "Expires", s2s(obj.Expires))
			setHeader(w, "Last-Modified", t2s(obj.LastModified))

			io.Copy(w, obj.Body)
			break
		}
	}
}

func challenge(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("WWW-Authenticate", `Basic realm="`+getHost(r)+`"`)
	http.Error(w, "", http.StatusUnauthorized)
}

func getHost(r *http.Request) string {
	host := r.Header.Get("Host")
	if host == "" {
		host = r.Host
	}

	return host
}

func s2s(s *string) string {
	if s != nil {
		return *s
	}
	return ""
}

func i2s(i *int64) string {
	if i != nil {
		return strconv.FormatInt(*i, 10)
	}
	return ""
}

func t2s(t *time.Time) string {
	if t != nil {
		return t.UTC().Format(http.TimeFormat)
	}
	return ""
}

func setHeader(w http.ResponseWriter, key, value string) {
	if value != "" {
		w.Header().Add(key, value)
	}
}
