module gitlab.com/druppy/s3-proxy

go 1.14

require (
	github.com/aws/aws-sdk-go v1.35.35
	github.com/gorilla/handlers v1.5.1
	github.com/justinas/alice v1.2.0
	github.com/rs/zerolog v1.20.0
)
