package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"
	"strings"

	"github.com/gorilla/handlers"
	"github.com/rs/zerolog/log"
)

type sitesCfg []Site

const (
	configName         = "S3PROXY_CONFIG"
	awsKeyName         = "S3PROXY_AWS_KEY"
	awsSecretName      = "S3PROXY_AWS_SECRET"
	awsRegionName      = "S3PROXY_AWS_REGION"
	awsBucketName      = "S3PROXY_AWS_BUCKET"
	usersName          = "S3PROXY_USERS"
	optCorsKeyName     = "S3PROXY_OPTION_CORS"
	optGzipKeyName     = "S3PROXY_OPTION_GZIP"
	optWebsiteKeyName  = "S3PROXY_OPTION_WEBSITE"
	optPrefixKeyName   = "S3PROXY_OPTION_PREFIX"
	optForceSSLKeyName = "S3PROXY_OPTION_FORCE_SSL"
	optProxiedKeyName  = "S3PROXY_OPTION_PROXIED"
	optNotFoundURIName = "S3PROXY_OPTION_NOTFOUND_URI"
)

// ConfiguredProxyHandler loads config from either env or config file
func ConfiguredProxyHandler() (http.Handler, error) {
	if _, ok := os.LookupEnv(configName); ok {
		return createMulti()
	}

	return createSingle()
}

func createMulti() (http.Handler, error) {
	var cfg sitesCfg
	cfgJSON := os.Getenv(configName)

	err := json.Unmarshal([]byte(cfgJSON), &cfg)
	if err != nil {
		return nil, err
	}

	if len(cfg) == 0 {
		return nil, errors.New("Must specify one or more configurations")
	}

	handler := NewHostDispatchingHandler()

	for i, site := range cfg {
		err = site.validateWithHost()

		if err != nil {
			msg := fmt.Sprintf("%v in configuration at position %d", err, i)
			return nil, errors.New(msg)
		}

		handler.HandleHost(site.Host, createSiteHandler(site))
	}

	return handler, nil
}

func createSingle() (http.Handler, error) {
	users, err := parseUsers(os.Getenv(usersName))
	if err != nil {
		return nil, err
	}

	opts := Options{
		CORS:        os.Getenv(optCorsKeyName) == "true",
		Gzip:        os.Getenv(optGzipKeyName) == "true",
		Website:     os.Getenv(optWebsiteKeyName) == "true",
		Prefix:      os.Getenv(optPrefixKeyName),
		ForceSSL:    os.Getenv(optForceSSLKeyName) == "true",
		Proxied:     os.Getenv(optProxiedKeyName) == "true",
		NotFoundURI: os.Getenv(optNotFoundURIName),
	}

	s := Site{
		AWSKey:    os.Getenv(awsKeyName),
		AWSSecret: os.Getenv(awsSecretName),
		AWSRegion: os.Getenv(awsRegionName),
		AWSBucket: os.Getenv(awsBucketName),
		Users:     users,
		Options:   opts,
	}

	if err = s.validate(); err != nil {
		return nil, err
	}

	return createSiteHandler(s), nil
}

func createSiteHandler(s Site) http.Handler {
	var handler http.Handler

	proxy := NewS3Proxy(s)
	handler = NewProxyHandler(proxy, s.Options.Prefix)

	if s.Options.Website {
		cfg, err := proxy.GetWebsiteConfig()
		if err != nil {
			fmt.Printf("warning: site for bucket %s configured with "+
				"website option but received error when retrieving "+
				"website config\n\t%v", s.AWSBucket, err)
		} else {
			handler = NewWebsiteHandler(handler, cfg)
		}
	}

	if s.Options.CORS {
		handler = corsHandler(handler)
	}

	if s.Options.Gzip {
		handler = handlers.CompressHandler(handler)
	}

	if len(s.Users) > 0 {
		handler = NewBasicAuthHandler(s.Users, handler)
	} else {
		fmt.Printf("warning: site for bucket %s has no configured users\n", s.AWSBucket)
	}

	if s.Options.ForceSSL {
		handler = NewSSLRedirectHandler(handler)
	}

	if s.Options.Proxied {
		handler = handlers.ProxyHeaders(handler)
	}

	return handler
}

func corsHandler(next http.Handler) http.Handler {
	return handlers.CORS(
		handlers.AllowedHeaders([]string{"*"}),
		handlers.AllowedOrigins([]string{"*"}),
		handlers.AllowedMethods([]string{"HEAD", "GET", "OPTIONS"}),
	)(next)
}

func parseUsers(us string) ([]User, error) {
	if us == "" {
		return []User{}, nil
	}

	pairs := strings.Split(us, ",")
	users := make([]User, len(pairs))

	for i, p := range pairs {
		parts := strings.Split(p, ":")
		if len(parts) != 2 {
			msg := fmt.Sprintf("Failed to parse user %s at position %d", p, i)
			return nil, errors.New(msg)
		}

		users[i] = User{
			Name:     parts[0],
			Password: parts[1],
		}
	}

	return users, nil
}

func (s Site) validateWithHost() error {
	if s.Host == "" {
		return errors.New("Host not specified")
	}

	return s.validate()
}

func (s Site) validate() error {
	if s.AWSKey != "" {
		log.Info().Msg("AWS key not defined, uses AIM roles")

		if s.AWSSecret == "" {
			return errors.New("AWS Secret not specified")
		}
	}

	if s.AWSRegion == "" {
		return errors.New("AWS Region not specified")
	}

	if s.AWSBucket == "" {
		return errors.New("AWS Bucket not specified")
	}

	return nil
}
