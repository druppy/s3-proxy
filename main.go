package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/justinas/alice"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/hlog"
	"github.com/rs/zerolog/log"
)

// Site define a single host related to a bucket
type Site struct {
	Host      string  `json:"host"`
	AWSKey    string  `json:"awsKey"`
	AWSSecret string  `json:"awsSecret"`
	AWSRegion string  `json:"awsRegion"`
	AWSBucket string  `json:"awsBucket"`
	Users     []User  `json:"users"`
	Options   Options `json:"options"`
}

// User defines basic auth for a given Site
type User struct {
	Name     string `json:"name"`
	Password string `json:"password"`
}

// Options defines options for a given site
type Options struct {
	CORS        bool   `json:"cors"`
	Gzip        bool   `json:"gzip"`
	Website     bool   `json:"website"`
	Prefix      string `json:"prefix"`
	ForceSSL    bool   `json:"forceSsl"`
	Proxied     bool   `json:"proxied"`
	NotFoundURI string `json:"notfound_uri"`
}

func requestLog(h http.Handler) http.Handler {
	log := zerolog.New(os.Stdout).With().
		Timestamp().
		Str("role", "s3-proxy").
		Str("host", "*").
		Logger()

	c := alice.New()

	// Install the logger handler with default output on the console
	c = c.Append(hlog.NewHandler(log))

	// Install some provided extra handler to set some request's context fields.
	// Thanks to that handler, all our logs will come with some prepopulated fields.
	c = c.Append(hlog.AccessHandler(func(r *http.Request, status, size int, duration time.Duration) {
		hlog.FromRequest(r).Info().
			Str("method", r.Method).
			Stringer("url", r.URL).
			Int("status", status).
			Int("size", size).
			Dur("duration", duration).
			Msg("")
	}))
	c = c.Append(hlog.RemoteAddrHandler("ip"))
	c = c.Append(hlog.UserAgentHandler("user_agent"))
	c = c.Append(hlog.RefererHandler("referer"))
	c = c.Append(hlog.RequestIDHandler("req_id", "Request-Id"))

	return c.Then(h)
}

func main() {
	// zerolog.TimeFieldFormat = zerolog.TimeFormatUnix

	debug := flag.Bool("debug", false, "sets log level to debug")
	port := flag.Int("port", 8080, "Port to listen on")

	flag.Parse()

	// Default level for this example is info, unless debug flag is present
	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	if *debug {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}

	handler, err := ConfiguredProxyHandler()
	if err != nil {
		log.Fatal().Err(err).Msg("config error")
		return
	}

	log.Info().Msgf("s3-proxy is listening on port %d", *port)

	h := handler

	if *debug == true {
		h = requestLog(h)
	}

	if err := http.ListenAndServe(fmt.Sprintf(":%d", *port), h); err != nil {
		log.Fatal().Err(err).Msg("proxy termination error")
	}
}
