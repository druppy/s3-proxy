package main

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

// S3Proxy define the proxy bucket part
type S3Proxy interface {
	Get(key string) (*s3.GetObjectOutput, error)
	GetWebsiteConfig() (*s3.GetBucketWebsiteOutput, error)
	GetOptions() Options
}

type realS3Proxy struct {
	bucket  string
	s3      *s3.S3
	options Options
}

// NewS3Proxy creates a S3 proxy
func NewS3Proxy(s Site) S3Proxy {
	cfg := aws.Config{
		Region: aws.String(s.AWSRegion),
	}

	if s.AWSKey != "" {
		cfg.Credentials = credentials.NewStaticCredentials(s.AWSKey, s.AWSSecret, "")
	}

	sess := session.Must(session.NewSession(&cfg))

	return &realS3Proxy{
		bucket:  s.AWSBucket,
		s3:      s3.New(sess),
		options: s.Options,
	}
}

func (p *realS3Proxy) GetOptions() Options {
	return p.options
}

func (p *realS3Proxy) Get(key string) (*s3.GetObjectOutput, error) {
	req := &s3.GetObjectInput{
		Bucket: aws.String(p.bucket),
		Key:    aws.String(key),
	}

	return p.s3.GetObject(req)
}

func (p *realS3Proxy) GetWebsiteConfig() (*s3.GetBucketWebsiteOutput, error) {
	req := &s3.GetBucketWebsiteInput{
		Bucket: aws.String(p.bucket),
	}

	return p.s3.GetBucketWebsite(req)
}
